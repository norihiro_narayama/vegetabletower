using UnityEngine;
using VegetableTower.Managers;

namespace VegetableTower.Prefabs {

    public class VegetableSprite : MonoBehaviour {
        [SerializeField] SpriteRenderer _sprite;
        [SerializeField] Rigidbody2D _rigidbody2d;

        public Rigidbody2D Rigidbody2d {
            get {
                return _rigidbody2d;
            }
        }
        //野菜の状態をenumで定義
        public enum State { start, move, stop, destroy };
        State _state;
        public State state {
            get {
                return _state;
            }
            set {
                _state = value;
            }
        }

        public void SetImage() {
            //野菜の決定
            string vegetableName = GameManager.Instance.VegetableList[Random.Range(0, GameManager.Instance.VegetableList.Count)];

            _sprite.sprite = (Sprite)Resources.Load("images/" + vegetableName , typeof(Sprite));
        }


        void OnEnable() {
            _state = State.start;
        }
       
        void Update(){
            if (_state != State.destroy) {
                if (_rigidbody2d != null && _state == State.move) {
                    if (_rigidbody2d.IsSleeping()) {
                        _state = State.stop;

                        //一番高い高さを越えている時のみ最大の高さを変更する
                        if (GameManager.Instance.MaxHeight < gameObject.transform.localPosition.y){
                            GameManager.Instance.MaxHeight = gameObject.transform.localPosition.y;
                        }

                        //カメラの移動
                        if (gameObject.transform.localPosition.y > 1) {
                            Camera.main.transform.localPosition = new Vector3(0, gameObject.transform.localPosition.y, -10);
                        }
                        GameManager.Instance.CreateVegetable();
                    }
                }

                if (!GetComponent<Renderer>().isVisible && !_rigidbody2d.IsSleeping()) {
                    _state = State.destroy;
                    GameManager.Instance.GameOver();
                }
            }
        }

    }
}
