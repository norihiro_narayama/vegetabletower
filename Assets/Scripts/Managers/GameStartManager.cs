using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace VegetableTower.Managers {
    public class GameStartManager : MonoBehaviour {
        [SerializeField] Text _scoreText;
        //スタートボタンをクリック
        public void StartClick() {
            SceneManager.LoadScene("GameMain");
        }

        void Awake() {
            int score = PlayerPrefs.GetInt("HIGH_SCORE", 0);
            _scoreText.text = score.ToString();
        }
    }
}
