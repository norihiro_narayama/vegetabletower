﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VegetableTower.Prefabs;

namespace VegetableTower.Managers {
	public sealed class GameManager  : MonoBehaviour {
        [SerializeField] Text _scoreText;

        static GameManager _instance;

        VegetableSprite _vegetableSprite;
        public VegetableSprite VegetableSprite {
            get {
                return _vegetableSprite;
            }
        }

        bool _buttonClicked;
        public bool ButtonClicked {
            get {
                return _buttonClicked;
            }
            set {
                _buttonClicked = value;
            }
        }

        //現在の最大の高さ
        float _maxHeight;
        public float MaxHeight {
            get {
                return _maxHeight;
            }
            set {
                _maxHeight = value;
            }
        }

        //野菜のリスト
        List<string> _vegetableList = new List<string>();
        public List<string> VegetableList {
            get {
                return _vegetableList;
            }
        }

        //初期化用に野菜を格納しておく用
        List<VegetableSprite> _vegetableSpriteList = new List<VegetableSprite>();

        //スコア
        int _score;
        public int Score {
            get {
                return _score;
            }
        }

        public static GameManager Instance {
            get {
                if (_instance == null) {
                    Debug.Log("GameManagerインスタンスがありません");
                }
                return _instance;
            }
        }

        void Awake() {
            if (_instance == null){
                _instance = this;
            }
            _maxHeight = -100;

            //野菜のリスト
            _vegetableList.Add("cabbage");
            _vegetableList.Add("tomato");

        }

        void Start(){
            CreateVegetable();
		}

        //野菜を生成する
        public void CreateVegetable(){

            //prefabを取得
            VegetableSprite prefab = (VegetableSprite)Resources.Load("Prefabs/VegetableSprite", typeof(VegetableSprite));
            //インスタンスを生成
            _vegetableSprite = Instantiate(prefab) as VegetableSprite;
            //格納リストに追加
            _vegetableSpriteList.Add(_vegetableSprite);
            //高さを算出
            _vegetableSprite.transform.position = CalcHeight();
            _vegetableSprite.Rigidbody2d.gravityScale = 0;
            _vegetableSprite.SetImage();
            //画像を読み込んでから、当たり判定を追加
            _vegetableSprite.gameObject.AddComponent<PolygonCollider2D>();

            //スコア加算
            if (_vegetableSpriteList.Count > 1){
                _score++;
                _scoreText.text = _score.ToString();


                //スコアをPlayerPrefsに保存
                int saveScore = PlayerPrefs.GetInt("HIGH_SCORE", 0);
                if (saveScore < _score){
                    PlayerPrefs.SetInt("HIGH_SCORE", _score);
                }
            }

        }

        //回転をクリック
        public void RotateClick(){
            _buttonClicked = true;
            _vegetableSprite.transform.Rotate(new Vector3(0,0,30));
        }

        //ゲームオーバー
        public void GameOver() {
            //初期化

            Initialize();
        }
        //初期化
		void Initialize() {

            //野菜を消す
            foreach (VegetableSprite sprite in _vegetableSpriteList){
                Destroy(sprite.gameObject);
            }
            _vegetableSpriteList.Clear();

            //カメラ位置を戻す
            Camera.main.transform.localPosition = new Vector3(0, 0, -10);

            //スコアの表示
            _score = 0;
            _scoreText.text = _score.ToString();

            _maxHeight = -100;
            //野菜の生成
            CreateVegetable();
		}


		Vector3 CalcHeight(){

            //現在の最大の高さを算出
            if (_maxHeight > -3){
                return new Vector3(0, _maxHeight + 3, 0);
            }

            return Vector3.zero;

        }

	}
}
