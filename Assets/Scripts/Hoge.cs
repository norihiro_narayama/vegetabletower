using UnityEngine;
using System.Collections;

public class Hoge : MonoBehaviour{

    [SerializeField]
    SpriteRenderer HogeSpriteRenderer;

	// Use this for initialization
    void Start(){
        if (HogeSpriteRenderer != null) {
            HogeSpriteRenderer.sprite = null;
        }
	}

	// Update is called once per frame
	void Update(){
			
	}
}
