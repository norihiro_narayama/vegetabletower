using UnityEngine;
using TouchScript;
using VegetableTower.Managers;
using System;
using VegetableTower.Prefabs;

namespace VegetableTower {
    public class TouchDisplay : MonoBehaviour {

        Boolean touchFlug; //タップしたかどうかのフラグ
        void OnEnable() {
            if (TouchManager.Instance != null) {
                //イベントハンドラの登録
                TouchManager.Instance.PointersPressed += pointersPressedHandler;
                TouchManager.Instance.PointersUpdated += pointersUpdatedHandler;
                TouchManager.Instance.PointersReleased += pointersReleasedHandler;
            }
        }

        void OnDisable() {
            if (TouchManager.Instance != null) {
                TouchManager.Instance.PointersPressed -= pointersPressedHandler;
                TouchManager.Instance.PointersUpdated -= pointersUpdatedHandler;
                TouchManager.Instance.PointersReleased -= pointersReleasedHandler;
            }
        }
        void pointersPressedHandler(object sender, PointerEventArgs e) {
            if (GameManager.Instance.ButtonClicked == true) {
                return;
            }
            touchFlug = true;
        }
        void pointersUpdatedHandler(object sender, PointerEventArgs e) {
            if (GameManager.Instance.ButtonClicked == true){
                return;
            }
            if (touchFlug == true) {
                foreach (var pointer in e.Pointers) {
                    Vector3 screenToWorldPointPosition = Camera.main.ScreenToWorldPoint(pointer.Position);
                    GameManager.Instance.VegetableSprite.transform.position = new Vector3(screenToWorldPointPosition.x, GameManager.Instance.VegetableSprite.transform.position.y, GameManager.Instance.VegetableSprite.transform.position.z);
                }
            }
        }

        void pointersReleasedHandler(object sender, PointerEventArgs e) {
            touchFlug = false;
            if (GameManager.Instance.ButtonClicked == true) {
                GameManager.Instance.ButtonClicked = false;
                return;
            }
            //野菜を落とす
            GameManager.Instance.VegetableSprite.Rigidbody2d.gravityScale = 1;
            GameManager.Instance.VegetableSprite.state = VegetableSprite.State.move;
        }
    }
}
